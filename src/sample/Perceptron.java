package sample;

import javax.sound.midi.Track;

/**
 * Created by KORN on 24.04.2017.
 */
public class Perceptron {
    private float[] weights; //Хранение весов
    public String label;
    final private static float THRESHOLD = 0.5f; //Пороговое значение

    Perceptron(String label) {
        this.label = label;
        this.weights = new float[Image.size()];

        for (int i = 0; i < weights.length; i++) {
            weights[i] = 0; //(float) (Math.random() - Math.random()); //-1;
        }
    }

    float sum(Image image) {
        float sum = 0;

        for (int i = 0; i < image.pixels.length; i++) {
            sum += image.pixels[i] * weights[i];
        }

        return sum;
    }

    void train(Image[] imagesForLearning) {
        boolean isDone = true;

        //Установка весов в цикле до тех пор, пока правильное изображение не будет получать верный результат
        //А неверное изображение не будет получать неправильный результат

        double dif;
        do  {
            isDone = true;
            for (Image image : imagesForLearning) {
                dif = THRESHOLD - sum(image);
                if (sum(image) < THRESHOLD && image.label.equals(this.label)) {

                    for (int i = 0; i < image.pixels.length; i++) {
                        if (image.pixels[i] == 1) {

                            weights[i] += 0.1 * dif * image.pixels[i];
                        }
                    }
                    isDone = false;
                } else if (sum(image) >= THRESHOLD && !image.label.equals(this.label)) {
                    for (int i = 0; i < image.pixels.length; i++) {
                        if (image.pixels[i] == 1) {
                            weights[i] += 0.1 * dif;
                        }
                    }
                    isDone = false;
                }
            }
        } while (!isDone);
    }
}
