package sample;

import com.csvreader.CsvReader;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.io.IOException;

/**
 * Created by KORN on 28.03.2017.
 */
public class Network {
    //Массивы:
    private Perceptron[] perceptrons = new Perceptron[10]; //перцептроны
    private Image[] trainingSet; //хранение обучающей выборки
    private Image[] testingSet; //хранение тестовой выборки
    boolean isTrained = false; //состояние обученности

    //Пороговое значение
    final private static float THRESHOLD = 0.5f;

    //Инициализатор сети с генерацией весов
    Network() throws IOException {
        for (int i = 0; i < perceptrons.length; i++) {
//            System.out.println(Integer.toString(i));
            perceptrons[i] = new Perceptron(Integer.toString(i));
        }
        getTrainingSet();
        getTestingSet();
    }

    private void getTrainingSet() throws IOException {
        try {
            CsvReader imagesCSV = new CsvReader("D:\\Учёба\\Магистратура\\Интеллектуальные системы\\NeuralNetwork\\src\\sample\\train.csv");
            imagesCSV.readHeaders();

            trainingSet = new Image[1000];
            String base = "pixel";

            for (int k = 0; k < trainingSet.length; k++) {
                trainingSet[k] = new Image();
                imagesCSV.readRecord();
                trainingSet[k].label = imagesCSV.get("label");
                trainingSet[k].pixels = new int[Image.size()];
                for (int i = 0; i < trainingSet[k].pixels.length; i++) {
                    trainingSet[k].pixels[i] = Integer.parseInt(imagesCSV.get(base.concat(Integer.toString(i)))) >= 1 ? 1 : 0;
                }

            }
        } finally {
//            for (int i = 0; i < Image.size(); i++) {
//                System.out.printf("%d ", trainingSet[1].pixels[i]);
//            }
        }

    }

    private void getTestingSet() throws IOException{
        try {
            CsvReader imagesCSV = new CsvReader("D:\\Учёба\\Магистратура\\Интеллектуальные системы\\NeuralNetwork\\src\\sample\\test.csv");
            imagesCSV.readHeaders();

            testingSet = new Image[1000];
            String base = "pixel";

            for (int k = 0; k < testingSet.length; k++) {
                testingSet[k] = new Image();
                imagesCSV.readRecord();
                testingSet[k].label = "unknown";
                testingSet[k].pixels = new int[Image.size()];
                for (int i = 0; i < testingSet[k].pixels.length; i++) {
                    testingSet[k].pixels[i] = Integer.parseInt(imagesCSV.get(base.concat(Integer.toString(i)))) >= 1 ? 1 : 0;
                }

            }
        } finally {

        }
    }

    //Обучение
    void train() {
        for (Perceptron p :
                perceptrons) {
            p.train(trainingSet);
        }
        this.isTrained = true;
    }

    void recognize() {
        for (Image image : testingSet) {
            int i = 0;
            while (image.label == "unknown") {
                if (perceptrons[i].sum(image) >= 0.5f) {
                    image.label = perceptrons[i].label;
                }
                i++;
                if (i > 9) break;
            }
        }
    }

    void recognize(int index) {
        Image image = testingSet[index];
        for (int i = 0; i < 10; i++) {
            if (perceptrons[i].sum(image) >= 0.5f) {
                image.label = perceptrons[i].label;
                if (image.label != "unknown") break;
            }
        }
    }

    public Image getImageFromTestingSetForIndex(int index) {
        return testingSet[index];
    }

    void check() {
        recognize();

        for (int i = 0; i < 10; i++) {
            System.out.println(testingSet[i].label);
        }
    }

    Node getImage(int imageIndexInSet, int scale) {
        Group group = new Group();

        Image imageToPlot = testingSet[imageIndexInSet];

        for (int i = 0; i < 28; i++) {
            for (int j = 0; j < 28; j++) {
                int index = i + j * 28;

                Rectangle rectangle = new Rectangle(i * scale, j * scale, scale, scale);

                if (imageToPlot.pixels[index] == 1) {
                    rectangle.setFill(Color.color(0, 0, 0));
                } else {
                    rectangle.setFill(Color.color(1, 1, 1));
                }

                group.getChildren().add(rectangle);
            }
        }

        return group;
    }
}
