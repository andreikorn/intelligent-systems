package sample;

import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends Application {

    private int currentImageIndex = 0;
    private Network network;

    @Override
    public void start(Stage primaryStage) throws Exception{

        this.network = new Network();

        BorderPane root = new BorderPane();

        //Создаём центр
        FlowPane flowPane = new FlowPane(10, 10);
        flowPane.setAlignment(Pos.CENTER);
        root.setCenter(flowPane);

        for (int i = 0; i < 50; i++) {
            flowPane.getChildren().add(network.getImage(i, 4));
        }

        //Создаём Header
        HBox headerBox = new HBox(10);
        headerBox.setPadding(new Insets(10, 10, 30, 10));
        headerBox.setAlignment(Pos.CENTER);

        Label header = new Label("Перед распознаванием необходимо обучить сеть: ");
        header.setAlignment(Pos.CENTER);
        header.setFont(Font.font("Calibri", FontWeight.BOLD, 24));

        Button trainingButton = new Button("Нажмите для обучения");
        trainingButton.setFont(Font.font("Calibri", FontWeight.BOLD, 16));

        trainingButton.setOnAction(actionEvent -> {
            ImageView imageView = new ImageView(new Image("sample/images/loading.gif"));
            imageView.setPreserveRatio(true);
            imageView.maxWidth(600);

            Task task = new Task<Void>() {
                @Override
                public Void call() throws Exception {

                    network.train();
                    updateProgress(10, 10);
                    System.out.println("Done");

                    return null;
                }

                @Override protected void succeeded() {
                    super.succeeded();

                    FadeTransition fadeTransition = new FadeTransition();
                    fadeTransition.setNode(imageView);
                    fadeTransition.setDuration(new Duration(1000));
                    fadeTransition.setFromValue(0.0);
                    fadeTransition.setToValue(1.0);
                    fadeTransition.setCycleCount(1);
                    fadeTransition.setOnFinished(actionEvent -> {
                        root.setCenter(getCentralNode());
                        header.setText("Теперь проверим работу нашей нейронной сети: ");
                        headerBox.getChildren().remove(1);
                    });
                    fadeTransition.play();


                    FadeTransition fadeTransitionTwo = new FadeTransition();
                    fadeTransitionTwo.setNode(root);
                    fadeTransitionTwo.setDuration(new Duration(1000));
                    fadeTransitionTwo.setFromValue(0.0);
                    fadeTransitionTwo.setToValue(1.0);
                    fadeTransitionTwo.setCycleCount(1);
                    fadeTransitionTwo.setDelay(new Duration(1000));
                    fadeTransitionTwo.play();
                    imageView.setImage(new Image("sample/images/done.png"));



                    network.check();

                    updateMessage("Done!");
                }

                @Override protected void cancelled() {
                    super.cancelled();
                    updateMessage("Cancelled!");
                }

                @Override protected void failed() {
                    super.failed();
                    updateMessage("Failed!");
                }
            };

            root.setCenter(imageView);

            new Thread(task).start();
        });

        headerBox.getChildren().addAll(header, trainingButton);

        root.setTop(headerBox);


        primaryStage.setTitle("Neural Network Numerals Recognition");
        primaryStage.setScene(new Scene(root, 788, 570));
        primaryStage.show();
    }

    Node getCentralNode() {

        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(0, 10, 0, 10));
        gridPane.setAlignment(Pos.TOP_CENTER);

        //добавляем картинку для распознавания
        gridPane.add(network.getImage(currentImageIndex, 13), 0, 0);

        //добавляем 2 кнопки под картинку
        HBox buttonBoxLeft = new HBox(10);
        buttonBoxLeft.setPadding(new Insets(10, 0, 0, 0));
        Button previousButton = new Button("Назад");
        previousButton.setFont(Font.font("Calibri", FontWeight.BOLD, 16));
        previousButton.setMinWidth(177);
        previousButton.setPrefWidth(177);
        previousButton.setMaxWidth(177);
        previousButton.setDisable(true);

        Button nextButton = new Button("Вперёд");
        nextButton.setFont(Font.font("Calibri", FontWeight.BOLD, 16));
        nextButton.setMinWidth(177);
        nextButton.setPrefWidth(177);
        nextButton.setMaxWidth(177);

        //добавляем действия при нажатии кнопок
        previousButton.setOnAction(actionEvent -> {
            currentImageIndex--;

            if (currentImageIndex == 0) {
                previousButton.setDisable(true);
            }

            if (currentImageIndex < 1000) {
                nextButton.setDisable(false);
            }

            gridPane.add(network.getImage(currentImageIndex, 13), 0, 0);
        });

        nextButton.setOnAction(actionEvent -> {
            currentImageIndex++;

            if (currentImageIndex > 0) {
                previousButton.setDisable(false);
            }

            if (currentImageIndex >= 1000) {
                nextButton.setDisable(true);
            }

            gridPane.add(network.getImage(currentImageIndex, 13), 0, 0);
        });

        buttonBoxLeft.getChildren().addAll(previousButton, nextButton);
        gridPane.add(buttonBoxLeft, 0, 1);

        //добавляем Label по середине
        Label middleLabel = new Label("→");
        middleLabel.setFont(Font.font("Calibri", FontWeight.BOLD, 16));
        middleLabel.setPadding(new Insets(0, 10, 0,10));
        middleLabel.setAlignment(Pos.CENTER);
        gridPane.add(middleLabel, 1,0);

        //добавляем картинки справа
        ImageView imageView = new ImageView();
        Image[] images = new Image[11];
        for (int i = 0; i < 10; i++) {
            images[i] = new Image("sample/images/" + i +".png");
        }
        images[10] = new Image("sample/images/unknown.png");
        imageView.setImage(images[10]);
        gridPane.add(imageView, 2,0);

        //кнопка для распознавания
        Button recognizeButton = new Button("Распознать");
        recognizeButton.setFont(Font.font("Calibri", FontWeight.BOLD, 16));
        recognizeButton.setMinWidth(364);
        recognizeButton.setPrefWidth(364);
        recognizeButton.setMaxWidth(364);
        HBox buttonBoxRight = new HBox(10);
        buttonBoxRight.setPadding(new Insets(10, 0, 0, 0));
        buttonBoxRight.getChildren().add(recognizeButton);
        gridPane.add(buttonBoxRight, 2, 1);

        recognizeButton.setOnAction(actionEvent -> {
            network.recognize(currentImageIndex);
//            System.out.println(currentImageIndex);
            sample.Image image = network.getImageFromTestingSetForIndex(currentImageIndex);
            System.out.println(image.label);

            if (image.label.equals("unknown")) {
                imageView.setImage(images[10]);
            } else {
                int number = Integer.parseInt(image.label);
                imageView.setImage(images[number]);
            }
        });

        return gridPane;
    }


    public static void main(String[] args) {
        launch(args);
    }
}
